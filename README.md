# Angular Dynamic Forms

Angular application with Dynamic Forms module and components

## To Start

Download and install node packages

```
npm install
```

Then run using

```
ng serve
```

Open your browser at http://localhost:4200/

### Contributions

Stewart Morgan, Digital Intelligence Unit, NHS Fylde Coast Clinical Commissioning Groups (stewart.morgan@nhs.net)

import { Component } from "@angular/core";

const shownMenuItems = [
  {
    state: "forms",
    name: "Forms",
    type: "link",
    icon: "how_to_vote",
  },
  {
    state: "designer",
    name: "Designer",
    type: "link",
    icon: "create",
  },
  {
    state: "list",
    name: "List",
    type: "link",
    icon: "all_inbox",
  },
];

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  shownMenuItems = shownMenuItems;
}

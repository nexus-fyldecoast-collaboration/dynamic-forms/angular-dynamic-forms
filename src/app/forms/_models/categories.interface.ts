export interface Categories {
  id: number;
  name: string;
  dateAdded: Date;
  deleteFlag: boolean;
  parent?: Categories;
}

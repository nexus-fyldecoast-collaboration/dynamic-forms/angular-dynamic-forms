import { Validators } from "@angular/forms";

export interface EntityForms {
  path: string;
  title: string;
  abbreviation: string;
  claim: string;
  id: string;
  questions: FieldConfig[];
  type: string;
}

export interface Validator {
  name: string;
  validator: Validators;
  message: string;
  validatortype: string;
  validatorpattern?: string;
}

export interface FieldConfig {
  label?: string;
  name?: string;
  inputType?: string;
  options?: Options[];
  collections?: any;
  type: string;
  value?: any;
  validators?: Validator[];
  savedDataType?: string;
  rules?: Rules[];
  orderNumber?: number;
  form?: EntityForms;
  globalList?: DTOGlobalList;
}

export interface Options {
  ordernumber?: number;
  option: string;
  id: any;
}

export interface Rules {
  dependOnQuestionID?: number;
  dependOnQuestionName: string;
  trigger: Trigger;
  action: string;
  actionValue: any;
  id: any;
}

export interface Trigger {
  type: string;
  value: any;
}

export interface DTOListItem {
  id: number;
  option: string;
  ordernumber: number;
  parentid: number;
}

export interface DTOQuestionList {
  entityGlobalListId: number;
  entityListId: number;
  list: DTOListItem;
}

export interface DTOGlobalList {
  id: number;
  name: string;
  description: string;
  deleteflag?: boolean;
  questionList?: DTOQuestionList[];
}

export interface FormTypes {
  index: string;
  Type: string;
  Created: Date;
}

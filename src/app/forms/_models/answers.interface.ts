import { FieldConfig } from "./field.interface";

export interface FormAnswers {
  id: number;
  answer: string;
  answerDT?: Date;
  answerNumber?: number;
  answerBool?: boolean;
  question: FieldConfig;
  groupedResponseID: number;
}

export interface GroupedResponse {
  id: number;
  submittedBy?: string;
  submittedDT?: Date;
  hospitalNumber?: string;
  otherUniqueID?: string;
  answers: FormAnswers[];
}

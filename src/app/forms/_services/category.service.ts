import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Categories } from "../_models/categories.interface";

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  baseUrl = environment.baseUrl;
  controller = "Categories";

  constructor(private http: HttpClient) {}

  public get() {
    return this.http.get<Categories[]>(this.baseUrl + this.controller + "/");
  }

  public add(payload: Categories) {
    return this.http.post(this.baseUrl + this.controller + "/", payload);
  }

  public remove(payload: Categories) {
    return this.http.delete(this.baseUrl + this.controller + "/" + payload.id);
  }

  public update(payload: Categories) {
    return this.http.put(
      this.baseUrl + this.controller + "/" + payload.id,
      payload
    );
  }
}

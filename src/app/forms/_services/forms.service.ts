import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { EntityForms } from "../_models/field.interface";

@Injectable({
  providedIn: "root",
})
export class FormsService {
  baseUrl = environment.baseUrl;
  controller = "Forms";

  constructor(private http: HttpClient) {}

  public get() {
    return this.http.get<EntityForms[]>(this.baseUrl + this.controller + "/");
  }

  public getBasic() {
    return this.http.get<EntityForms[]>(
      this.baseUrl + this.controller + "/getAll/"
    );
  }

  public getForm(id: number) {
    return this.http.get<EntityForms>(
      this.baseUrl + this.controller + "/" + id
    );
  }

  public add(payload: EntityForms) {
    return this.http.post(this.baseUrl + this.controller + "/", payload);
  }

  public remove(payload: EntityForms) {
    return this.http.delete(this.baseUrl + this.controller + "/" + payload.id);
  }

  public update(payload: EntityForms) {
    return this.http.put(
      this.baseUrl + this.controller + "/" + payload.id,
      payload
    );
  }
}

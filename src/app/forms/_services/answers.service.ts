import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { FormAnswers } from "../_models/answers.interface";

@Injectable({
  providedIn: "root",
})
export class FormAnswersService {
  baseUrl = environment.baseUrl;
  controller = "FormAnswers";

  constructor(private http: HttpClient) {}

  public get() {
    return this.http.get<FormAnswers[]>(this.baseUrl + this.controller + "/");
  }

  public getAnswersByGroupedResponseID(id: number) {
    return this.http.get<FormAnswers[]>(
      this.baseUrl +
        this.controller +
        "/getAnswersByGroupedResponseID?ResponseID=" +
        id
    );
  }

  public getFormAnswer(id: number) {
    return this.http.get<FormAnswers>(
      this.baseUrl + this.controller + "/" + id
    );
  }

  public add(payload: FormAnswers) {
    return this.http.post(this.baseUrl + this.controller + "/", payload);
  }

  public setAnswersByGroupedResponseID(payload: FormAnswers[]) {
    return this.http.post(
      this.baseUrl + this.controller + "/setAnswersByGroupedResponseID/",
      payload
    );
  }

  public remove(payload: FormAnswers) {
    return this.http.delete(this.baseUrl + this.controller + "/" + payload.id);
  }

  public update(payload: FormAnswers) {
    return this.http.put(
      this.baseUrl + this.controller + "/" + payload.id,
      payload
    );
  }
}

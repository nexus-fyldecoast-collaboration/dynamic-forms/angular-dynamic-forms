import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class DataService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public get(controller: string) {
    return this.http.get(this.baseUrl + controller + "/");
  }

  public getByID(controller: string, id: number) {
    return this.http.get(this.baseUrl + controller + "/" + id);
  }

  public getListByID(controller: string, id: number) {
    return this.http.get(this.baseUrl + controller + "/getListByID/" + id);
  }

  public add(controller: string, payload) {
    return this.http.post(this.baseUrl + controller + "/", payload);
  }

  public remove(controller: string, payload) {
    return this.http.delete(this.baseUrl + controller + "/" + payload.id);
  }

  public update(controller: string, payload) {
    return this.http.put(this.baseUrl + controller + "/" + payload.id, payload);
  }
}

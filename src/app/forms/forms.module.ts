import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsRoutes } from "./forms.routing";
import { RouterModule } from "@angular/router";
import { DesignerComponent } from "./designer/designer.component";
import { ListComponent } from "./list/list.component";
import { FormsComponent } from "./forms/forms.component";
import { MaterialModule } from "../material.module";
import { FormAnswersService } from "./_services/answers.service";
import { CategoryService } from "./_services/category.service";
import { DataService } from "./_services/data.service";
import { FormsService } from "./_services/forms.service";
import { ComponentsModule } from "./_components/components.module";
import { ContextService } from "./_services/context.service";
import { ReactiveFormsModule } from "@angular/forms";
import { ViewFormComponent } from "./designer/view-form/view-form.component";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FormsRoutes),
    MaterialModule,
    ComponentsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
  ],
  declarations: [
    DesignerComponent,
    FormsComponent,
    ListComponent,
    ViewFormComponent,
  ],
  exports: [DesignerComponent, FormsComponent, ListComponent],
  providers: [
    FormAnswersService,
    CategoryService,
    DataService,
    FormsService,
    ContextService,
  ],
})
export class FormsModule {}

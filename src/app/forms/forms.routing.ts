import { Routes } from "@angular/router";
import { DesignerComponent } from "./designer/designer.component";
import { ListComponent } from "./list/list.component";
import { FormsComponent } from "./forms/forms.component";

export const FormsRoutes: Routes = [
  {
    path: "forms",
    pathMatch: "full",
    component: FormsComponent,
  },
  {
    path: "designer",
    pathMatch: "full",
    component: DesignerComponent,
  },
  {
    path: "list",
    pathMatch: "full",
    component: ListComponent,
  },
];

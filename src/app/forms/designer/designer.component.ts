import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormArray,
  FormBuilder,
} from "@angular/forms";
import { EntityForms, FormTypes } from "../_models/field.interface";
import { FormsService } from "../_services/forms.service";
import { NotificationService } from "src/app/_services/notification.service";

@Component({
  selector: "app-designer",
  templateUrl: "./designer.component.html",
  styleUrls: ["./designer.component.css"],
})
export class DesignerComponent implements OnInit {
  formArray: EntityForms[];
  formtypes: FormTypes[];
  preview: EntityForms;
  newForm: FormGroup;

  constructor(
    private formsService: FormsService,
    private formBuilder: FormBuilder,
    private toastr: NotificationService
  ) {}

  ngOnInit() {
    this.refreshForms();
    this.newForm = this.formBuilder.group({
      title: null,
      abbreviation: null,
      claims: null,
      type: null,
      questions: this.formBuilder.array([this.createQuestion()]),
    });
    // this.newForm.valueChanges.subscribe(data => {
    //   console.log(data);
    // });
  }

  refreshForms() {
    this.formsService.get().subscribe((data) => {
      this.formArray = data.reverse();
    });
  }

  questionData(form) {
    return form.controls.questions.controls;
  }

  questionOptionsData(form) {
    return form.controls.options.controls;
  }

  questionRulesData(form) {
    return form.controls.rules.controls;
  }

  questionTriggerData(form) {
    return form.controls.trigger.controls;
  }

  questionValidationData(form) {
    return form.controls.validators.controls;
  }

  createQuestion(): FormGroup {
    return this.formBuilder.group({
      label: "",
      type: "",
      inputType: "",
      name: "",
      validators: this.formBuilder.array([]),
      options: this.formBuilder.array([]),
      rules: this.formBuilder.array([]),
    });
  }

  createValidator(): FormGroup {
    return this.formBuilder.group({
      name: "",
      validatorpattern: "",
      message: "",
    });
  }

  createOption(): FormGroup {
    return this.formBuilder.group({
      option: "",
      ordernumber: "",
    });
  }

  createRule(): FormGroup {
    return this.formBuilder.group({
      dependOnQuestionID: "",
      trigger: this.formBuilder.array([this.createTrigger()]),
      action: "",
      actionValue: "",
    });
  }

  createTrigger(): FormGroup {
    return this.formBuilder.group({
      type: "",
      value: "",
    });
  }

  addQuestion(): void {
    const questions = this.newForm.get("questions") as FormArray;
    questions.push(this.createQuestion());
  }

  removeQuestion(index: number): void {
    const questions = this.newForm.get("questions") as FormArray;
    questions.removeAt(index);
  }

  addQuestionValidator(index): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[index].get(
        "validators"
      )
    );
    control.push(this.createValidator());
  }

  removeQuestionValidator(index: number, i: number): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[i].get("validators")
    );
    control.removeAt(index);
  }

  addQuestionOption(index): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[index].get("options")
    );
    control.push(this.createOption());
  }

  removeQuestionOption(index: number, i: number): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[i].get("options")
    );
    control.removeAt(index);
  }

  addQuestionRule(index): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[index].get("rules")
    );
    control.push(this.createRule());
  }

  removeQuestionRule(index: number, i: number): void {
    const control = <FormArray>(
      (<FormArray>this.newForm.get("questions")).controls[i].get("rules")
    );
    control.removeAt(index);
  }

  previewForm(formValue: any) {
    this.preview = formValue;
    this.toastr.info("Preview loaded");
  }

  onSubmit(formValue: any) {
    this.preview = formValue;
    this.formsService.add(this.preview).subscribe((data) => {
      this.resetNewForm();
      this.refreshForms();
      this.toastr.success(
        "This form has been saved and can now be accessed by others."
      );
    });
  }

  resetNewForm() {
    this.preview = null;
    this.newForm.reset();
    this.newForm = this.formBuilder.group({
      title: "",
      abbreviation: "",
      claims: "",
      questions: this.formBuilder.array([this.createQuestion()]),
    });
  }
}

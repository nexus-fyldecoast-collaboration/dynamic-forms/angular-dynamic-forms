import { Component, OnInit, Input } from "@angular/core";
import { EntityForms } from "../../_models/field.interface";

@Component({
  selector: "app-view-form",
  templateUrl: "./view-form.component.html",
  styleUrls: ["./view-form.component.css"],
})
export class ViewFormComponent implements OnInit {
  @Input() selectedForm: EntityForms;
  constructor() {}

  ngOnInit() {}
}

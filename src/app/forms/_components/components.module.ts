import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { NgModule } from "@angular/core";
import { DynamicFieldDirective } from "./form-content/dynamic-field/dynamic-field.directive";
import { DynamicFormComponent } from "./form-content/dynamic-form/dynamic-form.component";
import { RadiobuttonComponent } from "./form-content/radiobutton/radiobutton.component";
import { SelectComponent } from "./form-content/select/select.component";
import { ButtonComponent } from "./form-content/button/button.component";
import { DateComponent } from "./form-content/date/date.component";
import { InputComponent } from "./form-content/input/input.component";
import { CheckboxComponent } from "./form-content/checkbox/checkbox.component";
import { RepeaterTableComponent } from "./form-content/repeater-table/repeater-table.component";
import { SignatureComponent } from "./form-content/signature/signature.component";
import { SignaturePadModule } from "angular-signaturepad";
import { FormParagraphComponent } from "./form-content/formparagraph/formparagraph.component";
import { FormHeaderComponent } from "./form-content/formheader/formheader.component";
import { FormLinkComponent } from "./form-content/formlink/formlink.component";
import { FormBulletListComponent } from "./form-content/formbulletlist/formbulletlist.component";
import { FormNumberListComponent } from "./form-content/formnumberlist/formnumberlist.component";
import { MaterialModule } from "src/app/material.module";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    SignaturePadModule,
  ],
  declarations: [
    DynamicFieldDirective,
    DynamicFormComponent,
    RadiobuttonComponent,
    SelectComponent,
    ButtonComponent,
    CheckboxComponent,
    DateComponent,
    InputComponent,
    RepeaterTableComponent,
    SignatureComponent,
    FormParagraphComponent,
    FormHeaderComponent,
    FormLinkComponent,
    FormBulletListComponent,
    FormNumberListComponent,
  ],
  entryComponents: [
    RadiobuttonComponent,
    SelectComponent,
    ButtonComponent,
    CheckboxComponent,
    DateComponent,
    InputComponent,
    RepeaterTableComponent,
    SignatureComponent,
    FormParagraphComponent,
    FormHeaderComponent,
    FormLinkComponent,
    FormBulletListComponent,
    FormNumberListComponent,
  ],
  exports: [DynamicFormComponent, RepeaterTableComponent],
})
export class ComponentsModule {}
